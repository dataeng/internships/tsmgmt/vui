# vui

A visual UI for selecting signals where an analysis is to be applied

Access the visual UI by: http://18.185.240.130:31861/

This visual UI was developed through the use of Grafana. The UI supports the graphical representation of the values of the 'analytics' simulated device, the accelerometer data of the ADL dataset (which can be seen at devices iot-1 and iot-2) and similar signal spans created for the Random and the ADL data.

For a detailed walkthrough on how to install Grafana and display the data we need, you may view the Grafana-Specifics.txt.

Now here are the dashboards created from the Grafana service.

Important note regarding time ranges: Selecting the time ranges you want can be a bit tricky, so remember the following steps. 
1) First, select the variables you want. (If the dashboard has any)
2) Next, set the time range using the time tables provided by the dashboard.
3) Finally, if you want to keep this range whenever you revisit the dashboard, remember to save the dashboard (both the variables and the time range).

`Random Data`

Displays the Random data from the database.

Panels:

- Random Data Graph (The graph with the values)
- Temperature Times (Time table)
- pH Times (Time table)
- Analytics Times (Time table)


`ADL Accelerometer`

Displays the ADL data from the database.

Variables:

- Volunteer ID (Volunteers recorded in the database)

Panels:

- ADL Data (The graph with the values)
- ADL Times (Time table)

`Similar Signal Spans (Random data)`

A query used to find similar signal spans with.

Variables:

- Field (Attribute)
- Segment (Segments recorded)

Panels:

- DTW Query (The graph with the values)
- DTW Query Times (Time table)


`Similar Signal Spans Match (Random data)`

A slice that matched a similar signal spans query.

Variables:

- Field (Attribute)
- Segment (Segments recorded)
- Slice (Segment slices recorded)

Panels:

- DTW Slice (The graph with the values)
- DTW Slice Times (Time table)
- DTW Difference and Threshold (Table that shows the difference and the threshold given from the query)


`Similar Signal Spans (ADL data)`

A segmented query to be classified.

Variables:

- Volunteer ID (Volunteers recorded in the database)
- Segment (Segments recorded)

Panels:

- ADL Classify Query (The graph with the values)
- ADL Classify Times (Time table)


`Similar Signal Spans Match (ADL data)`

The closest match of a given segment.

Variables:

- Volunteer ID (Volunteers recorded in the database)
- Segment (Segments recorded)


Panels:

- ADL Match (The graph with the values)
- ADL Match Times (Time table)
- Labels (Table that gives us the matched label and the actual label)
- DTW Difference (The recorded difference)


For the similar signals you may also use the Playlists provided to view the dashboards in a somewhat flexible manner.

Final note about variables: All variables are defined as 'multi-value' vars but this is done due to an error by Grafana. It is NOT advised to mix values, because the behavior is unexpected.
